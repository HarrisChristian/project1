# Employee Reimbursement System

## Description

This project is a full stack web application which configures an empoyee reimbursement system. Employees can log into the system online and view their requests as well as create new ones. Managers can do the same with the added feature of approving or denying requests from other employees.

## Technologies Uses

* Java
* AWS
* JDBC
* Tomcat
* AJAX
* HTML/CSS
* Javascript
* Servlets
* SQL
* Git
* Maven

## Features

1. Employees and managers can login through a web page.
2. Employees can view their old requests and filter the view.
3. Employees can make new requests.
4. Managers have all the features of an employee.
5. Managers can approve/deny requests from other employees.
6. Employees an managers can logout.
7. If a user attempts to access a page without an authorization token they are automatically redirected to the login page.

## To Do

* Implement hash and salt for passwords.
* Add in user profile information.
* Add in ability to upload receipt images and save them in database.
* Add in email notifications.
* Add in employee registration where employees are emailed temporary passwords.

## Getting Started

### Cloning
Navigate to desired directory and run:

git clone https://gitlab.com/HarrisChristian/project1.git

This project contains maven configurations and requires an active database to run. The database creation script is located in the src/main/resources/scripts directory. Then you will need to configure the credentials using environment variables as they are devined in the DatabaseUtil clas. The variables are

AWS_DB_URL
AWS_DB_USERNAME
AWS_DB_PASSWORD

Finally you will need to configure a tomcat server to host the application. Once configured and running you will be able to load a webpage using a link similar to the following.

http://localhost:8082/project1/static/html/landing_page.html

The database does not come configured with any users so you may need to manually enter one to login.



