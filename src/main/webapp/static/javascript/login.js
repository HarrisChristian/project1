document.getElementById('btn-login-submit').addEventListener('click', attemptLogin);

const baseHtmlUrl = "http://localhost:8082/project1/static/html";

function attemptLogin(){
    let username = document.getElementById('username-input').value;
    let password = document.getElementById('password-input').value;
    let credentials = {username, password};
    const loginUrl = baseUrl + "/login";
    performAjaxPostRequest(loginUrl, JSON.stringify(credentials), handleSuccessfulLogin, handleUnsuccessfulLogin);
}

function handleSuccessfulLogin(responseText){
    document.getElementById("error-msg").hidden = true;
    let token = responseText;
    sessionStorage.setItem("token", token);
	let userType = token.split(":")[1];
	let newPageUrl = baseHtmlUrl + "/home.html";
    window.location = newPageUrl;
}

function handleUnsuccessfulLogin(){
    document.getElementById("error-msg").hidden = false;
}