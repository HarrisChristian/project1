package dev.harris.user;

/**
 * Represents an employee object for a typical organization.
 *
 * @author Christian Harris
 *
 */
public class Employee {

    private String id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String dateOfBirth;
    private String email;
    private String phoneNumber;
    private boolean isAManager;

    public Employee() {
	this.id = null;
	this.username = null;
	this.password = null;
	this.firstName = null;
	this.lastName = null;
	this.middleName = null;
	this.dateOfBirth = null;
	this.email = null;
	this.phoneNumber = null;
	this.isAManager = false;
    }

    public Employee(String id, String username, String password, String firstName, String lastName, String middleName,
	    String dateOfBirth, String email, String phoneNumber, boolean isAManager) {
	this.id = id;
	this.username = username;
	this.password = password;
	this.firstName = firstName;
	this.lastName = lastName;
	this.middleName = middleName;
	this.dateOfBirth = dateOfBirth;
	this.email = email;
	this.phoneNumber = phoneNumber;
	this.isAManager = isAManager;
    }

    public String getId() {
	return this.id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getUsername() {
	return this.username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return this.password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getFirstName() {
	return this.firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return this.lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getMiddleName() {
	return this.middleName;
    }

    public void setMiddleName(String middleName) {
	this.middleName = middleName;
    }

    public String getDateOfBirth() {
	return this.dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPhoneNumber() {
	return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
    }

    public boolean isAManager() {
	return this.isAManager;
    }

    public boolean getIsAManager() {
	return this.isAManager;
    }

    public void setAManager(boolean isAManager) {
	this.isAManager = isAManager;
    }

    @Override
    public String toString() {
	return "Employee [id=" + this.id + ", username=" + this.username + ", password=" + this.password
		+ ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", middleName=" + this.middleName
		+ ", dateOfBirth=" + this.dateOfBirth + ", email=" + this.email + ", phoneNumber=" + this.phoneNumber
		+ ", isAManager=" + this.isAManager + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = (prime * result) + ((this.dateOfBirth == null) ? 0 : this.dateOfBirth.hashCode());
	result = (prime * result) + ((this.email == null) ? 0 : this.email.hashCode());
	result = (prime * result) + ((this.firstName == null) ? 0 : this.firstName.hashCode());
	result = (prime * result) + ((this.id == null) ? 0 : this.id.hashCode());
	result = (prime * result) + (this.isAManager ? 1231 : 1237);
	result = (prime * result) + ((this.lastName == null) ? 0 : this.lastName.hashCode());
	result = (prime * result) + ((this.middleName == null) ? 0 : this.middleName.hashCode());
	result = (prime * result) + ((this.password == null) ? 0 : this.password.hashCode());
	result = (prime * result) + ((this.phoneNumber == null) ? 0 : this.phoneNumber.hashCode());
	result = (prime * result) + ((this.username == null) ? 0 : this.username.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (this.getClass() != obj.getClass()) {
	    return false;
	}
	final Employee other = (Employee) obj;
	if (this.dateOfBirth == null) {
	    if (other.dateOfBirth != null) {
		return false;
	    }
	} else if (!this.dateOfBirth.equals(other.dateOfBirth)) {
	    return false;
	}
	if (this.email == null) {
	    if (other.email != null) {
		return false;
	    }
	} else if (!this.email.equals(other.email)) {
	    return false;
	}
	if (this.firstName == null) {
	    if (other.firstName != null) {
		return false;
	    }
	} else if (!this.firstName.equals(other.firstName)) {
	    return false;
	}
	if (this.id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!this.id.equals(other.id)) {
	    return false;
	}
	if (this.isAManager != other.isAManager) {
	    return false;
	}
	if (this.lastName == null) {
	    if (other.lastName != null) {
		return false;
	    }
	} else if (!this.lastName.equals(other.lastName)) {
	    return false;
	}
	if (this.middleName == null) {
	    if (other.middleName != null) {
		return false;
	    }
	} else if (!this.middleName.equals(other.middleName)) {
	    return false;
	}
	if (this.password == null) {
	    if (other.password != null) {
		return false;
	    }
	} else if (!this.password.equals(other.password)) {
	    return false;
	}
	if (this.phoneNumber == null) {
	    if (other.phoneNumber != null) {
		return false;
	    }
	} else if (!this.phoneNumber.equals(other.phoneNumber)) {
	    return false;
	}
	if (this.username == null) {
	    if (other.username != null) {
		return false;
	    }
	} else if (!this.username.equals(other.username)) {
	    return false;
	}
	return true;
    }
}
