package dev.harris.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import dev.harris.enums.ReimbursementCategory;

public class ReimbursementRequest {
    private int id;
    private String employeeId;
    private String managerId;
    private String dateMade;
    private String dateResolved;
    private String description;
    private ReimbursementCategory category;
    private BigDecimal cost;
    private boolean approved;

    public ReimbursementRequest() {
	super();
	this.id = -1;
	this.employeeId = null;
	this.managerId = null;
	this.dateMade = LocalDate.now().toString();
	this.dateResolved = null;
	this.description = null;
	this.category = null;
	this.cost = null;
	this.approved = false;
    }

    public ReimbursementRequest(int id, String employeeId, String managerId, String dateMade, String dateResolved,
	    String description, ReimbursementCategory category, BigDecimal cost, boolean approved) {
	super();
	this.id = id;
	this.employeeId = employeeId;
	this.managerId = managerId;
	this.dateMade = dateMade;
	this.dateResolved = dateResolved;
	this.description = description;
	this.category = category;
	this.cost = cost;
	this.approved = approved;
    }

    public ReimbursementRequest(String employeeId, String description, String category, String cost) {
	super();
	this.id = -1;
	this.employeeId = employeeId;
	this.managerId = null;
	this.dateMade = LocalDate.now().toString();
	this.dateResolved = null;
	this.description = description;
	this.category = ReimbursementCategory.toReimbursementCategory(category);
	this.cost = new BigDecimal(cost);
	this.approved = false;
    }

    public int getId() {
	return this.id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getEmployeeId() {
	return this.employeeId;
    }

    public void setEmployeeId(String employeeId) {
	this.employeeId = employeeId;
    }

    public String getManagerId() {
	return this.managerId;
    }

    public void setManagerId(String managerId) {
	this.managerId = managerId;
    }

    public String getDateMade() {
	return this.dateMade;
    }

    public void setDateMade(String dateMade) {
	this.dateMade = dateMade;
    }

    public String getDateResolved() {
	return this.dateResolved;
    }

    public void setDateResolved(String dateResolved) {
	this.dateResolved = dateResolved;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public ReimbursementCategory getCategory() {
	return this.category;
    }

    public void setCategory(ReimbursementCategory category) {
	this.category = category;
    }

    public BigDecimal getCost() {
	return this.cost;
    }

    public void setCost(BigDecimal cost) {
	this.cost = cost;
    }

    public boolean getApproved() {
	return this.approved;
    }

    public void setApproved(boolean approved) {
	this.approved = approved;
    }

    @Override
    public String toString() {
	return "ReimbursementRequest [id=" + this.id + ", employeeId=" + this.employeeId + ", managerId="
		+ this.managerId + ", dateMade=" + this.dateMade + ", dateResolved=" + this.dateResolved
		+ ", description=" + this.description + ", category=" + this.category + ", cost=" + this.cost
		+ ", approved=" + this.approved + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = (prime * result) + (this.approved ? 1231 : 1237);
	result = (prime * result) + ((this.category == null) ? 0 : this.category.hashCode());
	result = (prime * result) + ((this.cost == null) ? 0 : this.cost.hashCode());
	result = (prime * result) + ((this.dateMade == null) ? 0 : this.dateMade.hashCode());
	result = (prime * result) + ((this.dateResolved == null) ? 0 : this.dateResolved.hashCode());
	result = (prime * result) + ((this.description == null) ? 0 : this.description.hashCode());
	result = (prime * result) + ((this.employeeId == null) ? 0 : this.employeeId.hashCode());
	result = (prime * result) + this.id;
	result = (prime * result) + ((this.managerId == null) ? 0 : this.managerId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (this.getClass() != obj.getClass()) {
	    return false;
	}
	final ReimbursementRequest other = (ReimbursementRequest) obj;
	if (this.approved != other.approved) {
	    return false;
	}
	if (this.category != other.category) {
	    return false;
	}
	if (this.cost == null) {
	    if (other.cost != null) {
		return false;
	    }
	} else if (!this.cost.equals(other.cost)) {
	    return false;
	}
	if (this.dateMade == null) {
	    if (other.dateMade != null) {
		return false;
	    }
	} else if (!this.dateMade.equals(other.dateMade)) {
	    return false;
	}
	if (this.dateResolved == null) {
	    if (other.dateResolved != null) {
		return false;
	    }
	} else if (!this.dateResolved.equals(other.dateResolved)) {
	    return false;
	}
	if (this.description == null) {
	    if (other.description != null) {
		return false;
	    }
	} else if (!this.description.equals(other.description)) {
	    return false;
	}
	if (this.employeeId == null) {
	    if (other.employeeId != null) {
		return false;
	    }
	} else if (!this.employeeId.equals(other.employeeId)) {
	    return false;
	}
	if (this.id != other.id) {
	    return false;
	}
	if (this.managerId == null) {
	    if (other.managerId != null) {
		return false;
	    }
	} else if (!this.managerId.equals(other.managerId)) {
	    return false;
	}
	return true;
    }

}
