package dev.harris.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class DatabaseUtil {
    private static Logger logger = Logger.getRootLogger();

    public static Connection connection;

    private DatabaseUtil() {
    };

    public static Connection getConnection() throws SQLException {
	logger.info("Retrieving database connection.");

	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	} catch (final ClassNotFoundException ex) {
	    ex.printStackTrace();
	}
	final String url = System.getenv("AWS_DB_URL"); // best not to hard code in your db credentials !
	final String username = System.getenv("AWS_DB_USERNAME");
	final String password = System.getenv("AWS_DB_PASSWORD");
	if ((connection == null) || connection.isClosed()) {
	    connection = DriverManager.getConnection(url, username, password);
	}
	return connection;
    }
}
