package dev.harris.dao;

import java.util.List;

import dev.harris.model.ReimbursementRequest;

public interface ReimbursementRequestDaoInterface {

    public List<ReimbursementRequest> getReimbursementRequests(String id, boolean pending, boolean resolved);

    public void insertNewReimbursementRequest(ReimbursementRequest reimbursementRequest);

    public List<ReimbursementRequest> getReimbursementRequestsForManagerApproval(String managerId);

    public void approveReimbursementRequest(String reimbursementRequestId, String managerId);

    public void denyReimbursementRequest(String reimbursementRequestId, String managerId);
}
