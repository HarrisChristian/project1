package dev.harris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dev.harris.enums.ReimbursementCategory;
import dev.harris.model.ReimbursementRequest;
import dev.harris.util.DatabaseUtil;

public class ReimbursementRequestDao implements ReimbursementRequestDaoInterface {

    private final Logger logger = Logger.getRootLogger();

    @Override
    public List<ReimbursementRequest> getReimbursementRequests(String employeeId, boolean pending, boolean resolved) {
	this.logger.info("Getting reimbursement requests.");
	final List<ReimbursementRequest> reimbursementRequests = new ArrayList<>();

	final String sql = "SELECT * FROM REIMBURSEMENT_REQUEST WHERE EMPLOYEE_ID = ?";

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, employeeId);
	    final ResultSet resultSet = pStatement.executeQuery();
	    while (resultSet.next()) {
		final String dateResolved = resultSet.getString("DATE_RESOLVED");
		if ((pending && (dateResolved == null)) || (resolved && (dateResolved != null))) {

		    reimbursementRequests
			    .add(new ReimbursementRequest(resultSet.getInt("ID"), resultSet.getString("EMPLOYEE_ID"),
				    resultSet.getString("MANAGER_ID"), resultSet.getString("DATE_MADE"),
				    resultSet.getString("DATE_RESOLVED"), resultSet.getString("DESCRIPTION"),
				    ReimbursementCategory.toReimbursementCategory(resultSet.getString("CATEGORY")),
				    resultSet.getBigDecimal("COST"),
				    ReimbursementRequestDao.convertApprovedToBoolean(resultSet.getString("APPROVED"))));
		}
	    }
	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}

	return reimbursementRequests;
    }

    @Override
    public void insertNewReimbursementRequest(ReimbursementRequest reimbursementRequest) {
	this.logger.info("Inserting reimbursement request");
	final String sql = "INSERT INTO REIMBURSEMENT_REQUEST (EMPLOYEE_ID, DATE_MADE, DESCRIPTION, CATEGORY, COST, APPROVED) values (?, ?, ?, ?, ?, ?)";

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    System.out.println(reimbursementRequest);
	    pStatement.setString(1, reimbursementRequest.getEmployeeId());
	    pStatement.setString(2, reimbursementRequest.getDateMade());
	    pStatement.setString(3, reimbursementRequest.getDescription());
	    pStatement.setString(4, reimbursementRequest.getCategory().toString());
	    pStatement.setBigDecimal(5, reimbursementRequest.getCost());
	    pStatement.setString(6,
		    ReimbursementRequestDao.convertBooleanToApproved(reimbursementRequest.getApproved()));
	    pStatement.executeUpdate();
	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}
    }

    @Override
    public List<ReimbursementRequest> getReimbursementRequestsForManagerApproval(String managerId) {
	this.logger.info("Getting all pending reimbursement requests.");
	final String sql = "SELECT * FROM REIMBURSEMENT_REQUEST WHERE REIMBURSEMENT_REQUEST.EMPLOYEE_ID != ? AND DATE_RESOLVED IS NULL";
	final List<ReimbursementRequest> reimbursementRequests = new ArrayList<>();

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, managerId);
	    final ResultSet resultSet = pStatement.executeQuery();
	    while (resultSet.next()) {
		reimbursementRequests
			.add(new ReimbursementRequest(resultSet.getInt("ID"), resultSet.getString("EMPLOYEE_ID"),
				resultSet.getString("MANAGER_ID"), resultSet.getString("DATE_MADE"),
				resultSet.getString("DATE_RESOLVED"), resultSet.getString("DESCRIPTION"),
				ReimbursementCategory.toReimbursementCategory(resultSet.getString("CATEGORY")),
				resultSet.getBigDecimal("COST"),
				ReimbursementRequestDao.convertApprovedToBoolean(resultSet.getString("APPROVED"))));
	    }
	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}
	return reimbursementRequests;
    }

    @Override
    public void approveReimbursementRequest(String reimbursementRequestId, String managerId) {
	this.logger.info("Approving a reimbursement request");
	final String sql = "UPDATE REIMBURSEMENT_REQUEST SET MANAGER_ID = ?, DATE_RESOLVED = ?, APPROVED = 'Y' WHERE ID = ?";
	final String dateResolved = LocalDate.now().toString();

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, managerId);
	    pStatement.setString(2, dateResolved);
	    pStatement.setString(3, reimbursementRequestId);
	    pStatement.executeUpdate();
	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}
    }

    @Override
    public void denyReimbursementRequest(String reimbursementRequestId, String managerId) {
	this.logger.info("Denying a reimbursement request");
	final String sql = "UPDATE REIMBURSEMENT_REQUEST SET MANAGER_ID = ?, DATE_RESOLVED = ?, APPROVED = 'N' WHERE ID = ?";
	final String dateResolved = LocalDate.now().toString();

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, managerId);
	    pStatement.setString(2, dateResolved);
	    pStatement.setString(3, reimbursementRequestId);
	    pStatement.executeUpdate();
	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}
    }

    private static boolean convertApprovedToBoolean(String s) {
	if (s.equals("Y")) {
	    return true;
	}
	return false;
    }

    private static String convertBooleanToApproved(boolean b) {
	if (b) {
	    return "Y";
	} else {
	    return "N";
	}
    }
}
