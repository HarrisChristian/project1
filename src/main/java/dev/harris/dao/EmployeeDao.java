package dev.harris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import dev.harris.user.Credentials;
import dev.harris.user.Employee;
import dev.harris.util.DatabaseUtil;

public class EmployeeDao implements EmployeeDaoInterface {

    private final Logger logger = Logger.getRootLogger();

    @Override
    public boolean validateCredentials(Credentials credentials) {
	this.logger.info("Validating credentials.");
	final String sql = "SELECT USERNAME, PASSWORD FROM EMPLOYEE WHERE EMPLOYEE.USERNAME = ?";

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, credentials.getUsername());
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next() && resultSet.getString("PASSWORD").equals(credentials.getPassword())) {
		return true;
	    }
	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}
	return false;
    }

    @Override
    public Employee getEmployeeByUsername(String username) {
	this.logger.info("Getting employee.");
	final Employee employee = null;
	final String sql = "SELECT * FROM EMPLOYEE WHERE USERNAME = ?";

	try (Connection connection = DatabaseUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next()) {
		return new Employee(resultSet.getString("ID"), resultSet.getString("USERNAME"),
			resultSet.getString("PASSWORD"), resultSet.getString("FIRST_NAME"),
			resultSet.getString("LAST_NAME"), resultSet.getString("MIDDLE_NAME"),
			resultSet.getString("DATE_OF_BIRTH"), resultSet.getString("EMAIL"),
			resultSet.getString("PHONE_NUMBER"),
			EmployeeDao.convertIsAManagerToBoolean(resultSet.getString("IS_MANAGER")));
	    }

	} catch (final SQLException ex) {
	    ex.printStackTrace();
	}
	return employee;
    }

    private static boolean convertIsAManagerToBoolean(String s) {
	if (s.equals("Y")) {
	    return true;
	}
	return false;
    }
}
