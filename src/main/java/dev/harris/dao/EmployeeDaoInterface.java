package dev.harris.dao;

import dev.harris.user.Credentials;
import dev.harris.user.Employee;

public interface EmployeeDaoInterface {

    public boolean validateCredentials(Credentials credentials);

    public Employee getEmployeeByUsername(String username);

}
