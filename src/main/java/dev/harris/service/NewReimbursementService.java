package dev.harris.service;

import dev.harris.dao.ReimbursementRequestDao;
import dev.harris.model.ReimbursementRequest;

public class NewReimbursementService {
    private final ReimbursementRequestDao reimbursementRequestDao = new ReimbursementRequestDao();

    public void insertNewReimbursementRequest(ReimbursementRequest reimbursementRequest) {
	this.reimbursementRequestDao.insertNewReimbursementRequest(reimbursementRequest);
    }
}
