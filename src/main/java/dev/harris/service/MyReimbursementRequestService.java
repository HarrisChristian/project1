package dev.harris.service;

import java.util.List;

import dev.harris.dao.ReimbursementRequestDao;
import dev.harris.model.ReimbursementRequest;

public class MyReimbursementRequestService {

    private final ReimbursementRequestDao reimbursementRequestDao = new ReimbursementRequestDao();

    public List<ReimbursementRequest> getReimbursementRequests(String employeeId, boolean pending, boolean resolved) {
	return this.reimbursementRequestDao.getReimbursementRequests(employeeId, pending, resolved);
    }
}
