package dev.harris.service;

import java.util.List;

import dev.harris.dao.ReimbursementRequestDao;
import dev.harris.model.ReimbursementRequest;

public class ManageReimbursementRequestService {

    private final ReimbursementRequestDao reimbursementRequestDao = new ReimbursementRequestDao();

    public List<ReimbursementRequest> getReimbursementRequestsForManagerApproval(String id) {
	return this.reimbursementRequestDao.getReimbursementRequestsForManagerApproval(id);
    }

    public void approveReimbursementRequest(String reimbursementRequestId, String managerId) {
	this.reimbursementRequestDao.approveReimbursementRequest(reimbursementRequestId, managerId);
    }

    public void denyReimbursementRequest(String reimbursementRequestId, String managerId) {
	this.reimbursementRequestDao.denyReimbursementRequest(reimbursementRequestId, managerId);
    }
}
