package dev.harris.service;

import dev.harris.dao.EmployeeDao;
import dev.harris.user.Credentials;
import dev.harris.user.Employee;

public class LoginService {

    private final EmployeeDao employeeDao = new EmployeeDao();

    public String validateCredentials(Credentials credentials) {
	String token = null;
	final boolean credentialsAreValid = this.employeeDao.validateCredentials(credentials);
	if (credentialsAreValid) {
	    final Employee employee = this.employeeDao.getEmployeeByUsername(credentials.getUsername());
	    token = employee.getId();
	    if (employee.isAManager()) {
		token += ":Manager";
	    } else {
		token += ":Employee";
	    }
	}
	return token;
    }
}
