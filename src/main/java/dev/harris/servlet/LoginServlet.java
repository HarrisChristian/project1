package dev.harris.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.harris.service.LoginService;
import dev.harris.user.Credentials;

public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final Logger logger = Logger.getRootLogger();
    private final LoginService loginService = new LoginService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	this.logger.info("Calling login servlet doPost.");
	final BufferedReader bufferedReader = request.getReader();
	String json = "";
	String line = bufferedReader.readLine();
	while (line != null) {
	    json += line;
	    line = bufferedReader.readLine();
	}
	final Credentials credentials = this.objectMapper.readValue(json, Credentials.class);
	final String token = this.loginService.validateCredentials(credentials);
	if (token != null) {
	    try (PrintWriter printWriter = response.getWriter()) {
		printWriter.write(token);
	    }
	} else {
	    response.sendError(401);
	}
    }
}
