package dev.harris.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.harris.model.ReimbursementRequest;
import dev.harris.service.MyReimbursementRequestService;

public class MyReimbursementRequestServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final Logger logger = Logger.getRootLogger();
    private final MyReimbursementRequestService reimbursementRequestService = new MyReimbursementRequestService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException {
	this.logger.info("Calling My Reimbursement Request Servlet doGet");
	final String selection = request.getParameter("selection");
	final String id = request.getParameter("id");
	boolean pending = false;
	boolean resolved = false;

	List<ReimbursementRequest> reimbursementRequests = new ArrayList<>();

	if ((selection != null) && (id != null)) {
	    if (selection.equals("all")) {
		pending = true;
		resolved = true;
	    } else if (selection.equals("pending")) {
		pending = true;
	    } else if (selection.equals("resolved")) {
		resolved = true;
	    }

	    reimbursementRequests = this.reimbursementRequestService.getReimbursementRequests(id, pending, resolved);

	    final String requestsJson = this.objectMapper.writeValueAsString(reimbursementRequests);
	    final PrintWriter pw = response.getWriter();
	    pw.write(requestsJson);
	    pw.close();

	} else {
	    response.sendError(400);
	}

    }
}
