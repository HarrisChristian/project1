package dev.harris.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.harris.model.ReimbursementRequest;
import dev.harris.service.ManageReimbursementRequestService;

public class ManageReimbursementRequestServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final Logger logger = Logger.getRootLogger();
    private final ManageReimbursementRequestService manageReimbursementRequestService = new ManageReimbursementRequestService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException {
	this.logger.info("Calling Manage Reimbursement Request Servlet doGet");
	final String id = request.getParameter("id");

	List<ReimbursementRequest> reimbursementRequests = new ArrayList<>();
	if (id != null) {
	    reimbursementRequests = this.manageReimbursementRequestService
		    .getReimbursementRequestsForManagerApproval(id);
	    final String requestsJson = this.objectMapper.writeValueAsString(reimbursementRequests);
	    final PrintWriter pw = response.getWriter();
	    pw.write(requestsJson);
	    pw.close();
	} else {
	    response.sendError(400);
	}
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	this.logger.info("Calling Manage Reimbursement Request Servlet doPut");
	final BufferedReader bufferedReader = request.getReader();
	String json = "";
	String line = bufferedReader.readLine();
	while (line != null) {
	    json += line;
	    line = bufferedReader.readLine();
	}
	final String type = request.getParameter("type");
	final String reimbursementRequestId = json.split(":")[0];
	final String managerId = json.split(":")[1];
	if ((type != null) && (json != null)) {
	    if (type.equals("approve")) {
		this.manageReimbursementRequestService.approveReimbursementRequest(reimbursementRequestId, managerId);
	    } else if (type.equals("deny")) {
		this.manageReimbursementRequestService.denyReimbursementRequest(reimbursementRequestId, managerId);
	    }

	} else {
	    response.sendError(400);
	}
    }

}
