package dev.harris.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.harris.model.ReimbursementRequest;
import dev.harris.service.NewReimbursementService;

public class NewReimbursementRequestServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private final Logger logger = Logger.getRootLogger();
    private final NewReimbursementService newReimbursementService = new NewReimbursementService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
	    throws IOException, ServletException {
	this.logger.info("Calling New Reimbursement Request Servlet doPut");
	final BufferedReader bufferedReader = request.getReader();
	String json = "";
	String line = bufferedReader.readLine();
	while (line != null) {
	    json += line;
	    line = bufferedReader.readLine();
	}
	final ReimbursementRequest reimbursementRequest = this.objectMapper.readValue(json, ReimbursementRequest.class);
	this.newReimbursementService.insertNewReimbursementRequest(reimbursementRequest);
    }
}
