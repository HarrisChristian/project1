package dev.harris.system;

import dev.harris.dao.ReimbursementRequestDao;
import dev.harris.model.ReimbursementRequest;

public class Test {

    public static void main(String[] args) {
	final ReimbursementRequest request = new ReimbursementRequest("abcd1234", "Testing new construction and insert",
		"Other", "3141.15");
	final ReimbursementRequestDao rrDao = new ReimbursementRequestDao();
	rrDao.insertNewReimbursementRequest(request);
	System.out.println("Exiting test...");
    }
}
